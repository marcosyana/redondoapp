import React from 'react';
import Header from './components/header'
import Services from './components/services'
import Hello from './components/hello'
import Soluciones from './components/soluciones'
import Empresa from './components/empresa'
import Precios from './components/precios'
import Contacto from './components/contacto'
import Footer from './components/footer'
function App() {
  return (
    <div className="App">
       <Header/>
       <Services/>
       <Hello/>
       <Soluciones/>
       <Empresa/>
       <Precios/>
       <Contacto/>
       <Footer/>
    </div>
  );
}

export default App;

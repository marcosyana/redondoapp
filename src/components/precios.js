import React from 'react' 
import './styles/precios.css'
export default class Precios extends React.Component{
    render(){
        return (
            <div className="container-images contenedor" id="precios">

            <h2> Nuestros Precios </h2>
    
            <div className="grid-precios2">
                <div className="item-precio2 _ip1">
                    <div className="contain-precio">
                        <p> $19 </p>
                    </div>
    
                    <p className="type"> Basic </p>
    
                    <ul className="list-acti">
                        <p className="li"> Atencion 24/7 </p>
                        <p className="li"> Soporte de hosting web </p>
    
                        <p className="li"> sertificado SSL </p>
    
                        <p className="li"> Cuentas Email </p>
    
                    </ul>
                    <div className="container-button">
                        <button className="comprar"> Elegir </button>
    
                    </div>
    
                </div>
                <div className="item-precio2 _ip2">
                    <div className="contain-precio">
                        <p> $29 </p>
                    </div>
    
                    <p className="type"> Standard </p>
    
                    <ul className="list-acti">
                        <p className="li"> Atencion 24/7 </p>
                        <p className="li"> Soporte de hosting web </p>
    
                        <p className="li"> sertificado SSL </p>
                        <p className="li"> Cuentas Email </p>
    
    
                    </ul>
                    <div className="container-button">
                        <button className="comprar"> Elegir </button>
    
                    </div>
                </div>
                <div className="item-precio2 _ip3">
                    <div className="contain-precio">
                        <p> $49 </p>
                    </div>
    
                    <p className="type"> Bussines </p>
    
                    <ul className="list-acti">
                        <p className="li"> Atencion 24/7 </p>
                        <p className="li"> Soporte de hosting web </p>
    
                        <p className="li"> sertificado SSL </p>
                        <p className="li"> Cuentas Email </p>
    
    
                    </ul>
                    <div className="container-button">
                        <button className="comprar"> Elegir </button>
    
                    </div>
                </div>
            </div>
    
    
        </div>
    
        )
    }
}
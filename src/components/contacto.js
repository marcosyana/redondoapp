import React from 'react'
import './styles/contacto.css'
export default class Contacto extends React.Component {
    render() {
        return (
            <div className="container-contact">
                <div className="contenedor">
                    <h4 className="centrar-texto"> Listo para empezar? </h4>
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem, totam provident.
                </p>
                    <div className="containerInputs">
                        <input type="text" placeholder="Nombre" />
                        <input type="email" placeholder="Email" />
                    </div>
                    <div className="contentButton">
                        <button>enviar</button>
                    </div>
                </div>
            </div>

        )
    }
}
import React from 'react'
import './styles/soluciones.css'
import i1 from '../images/ilustraciones/soluciones/_1.png'
import i2 from '../images/ilustraciones/soluciones/_2.png'
import i3 from '../images/ilustraciones/soluciones/_3.png'
import i4 from '../images/ilustraciones/soluciones/_4.png'
import i5 from '../images/ilustraciones/soluciones/_5.png'
import i6 from '../images/ilustraciones/soluciones/_7.png'
export default class Soluciones extends React.Component {
    render() {
        return (
            
            <div className="containerPortafolio contenedor" id="ilustraciones">

                <h2> Soluciones </h2>


                <div className="gridPorta">

                    <div className="itemP _p1">
                        <div className="container7 __1"> <img src={i1} alt="" /> </div>
                        <div className="container3">
                            <h4 className="centrar-texto"> Solución 1 </h4>
                            <p className="color1">
                                Blanditiis culpa reiciendisis.
                           </p>
                        </div>
                    </div>
                    <div className="itemP _p2">
                        <div className="container7 __2"> <img src={i2} alt="" /> </div>
                        <div className="container3">
                            <h4 className="centrar-texto"> Solución 2 </h4>
                            <p className="color1">
                                Blanditiis culpa reiciendisis.
                    </p>
                        </div>
                    </div>
                    <div className="itemP _p3">
                        <div className="container7 __3"> <img src={i3}  /> </div>
                            <div className="container3">
                                <h4 className="centrar-texto"> Solución 3</h4>
                                <p className="color1">
                                    Blanditiis culpa reiciendisis.
                    </p>
                            </div>
                        </div>
                        <div className="itemP _p4">
                            <div className="container7 __4"> <img src={i4} alt="" /> </div>
                            <div className="container3">
                                <h4 className="centrar-texto"> Solución 4 </h4>
                                <p className="color1">
                                    Blanditiis culpa reiciendisis.
                    </p>
                            </div>
                        </div>
                        <div className="itemP _p5">
                            <div className="container7 __5"> <img src={i5} alt="" /> </div>
                            <div className="container3">
                                <h4 className="centrar-texto"> Solución 5 </h4>
                                <p className="color1">
                                    Blanditiis culpa reiciendisis.
                    </p>
                            </div>
                        </div>


                        <div className="itemP _p6">
                            <div className="container7 __6"> <img src={i6} alt="/" /> </div>
                                <div className="container3">
                                    <h4 className="centrar-texto"> Solución 6 </h4>
                                    <p className="color1">
                                        Blanditiis culpa reiciendisis.
                                   </p>
                                </div>
                            </div>
                        </div>

                    </div>
        )
    }
} 
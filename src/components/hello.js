import React from 'react'
import './styles/hello.css'
import ilustrationHello from '../images/ilustraciones/11.svg'
export default class Hello extends React.Component{
    render(){
        return (
            <div className="containerHelloP" id="nosotros">

            <div className="containerHello contenedor">
    
                <div className="hello">
                    <h4> <span>Obten un</span> software a tu medida y <span className="span2">crea cosas increibles</span>
                    </h4>
    
                    <ul>
                        <li>
                            <p className="color1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatum illo possimus qui .
                            </p>
                        </li>
                        <li>
                            <p className="color1">Lorem ipsum dolor sit amet illo possimus qui elit. Voluptatum illo possimus.
                            </p>
                        </li>
                        <li>
                            <p className="color1"> adipisicing elit. Minima nulla dolorum suscipit amet mollitia nam dolores commodi. Veniam tempore facere.
                            </p>
                        </li>
                        <li>
                            <p className="color1">Lorem ipsum dolor sit amet illo possimus qui illo possimus.
                            </p>
                        </li>
                    </ul>
                </div>
                <div className="hello">
                    <div className="imageS _z1">
                        <img src={ilustrationHello} alt="" />
                    </div>
                </div>
    
            </div>
        </div>
        )
    }
}
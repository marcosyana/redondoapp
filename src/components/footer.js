import React from 'react'
import './styles/footer.css'
import Logito from '../images/logito.png'
export default class Footer extends React.Component {
    render() {
        return (
            <div className="side-footer contenedor">

                <a href="#home" className="boxlogo">
                    <img src={Logito} alt="" />
                    <p className="color1">Redondo</p>
                </a>

                <div className="box-red">
                    &copy; derechos reservados
                </div>

            </div>
        )
    }
}
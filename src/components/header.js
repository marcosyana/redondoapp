import React from 'react'
import sc from '../scrollReveal';

import './styles/header.css'
import logo from '../images/logito.png'

class sHeader extends React.Component {
    constructor(props){
        super(props);
        this.n1 = React.createRef()
    }
    componentDidMount = () =>{
        const config = {
            origin: 'bottom',
            duration: 400,
            delay:100,
            distance: '50px',
            easing: 'ease'
    }
    console.log(sc.reveal)
    }

    render() {
        return (
            <header className="side-header" id="home">
                <nav className="navegacion">
                    <div className="containLogo">
                         <img src={logo} className="logo" alt="Hello" />
                        <p>Redondo</p>
                    </div>
                    <div className="containMenu">
                        <a href="#servicios">Servicios <div className="linea"></div> </a>
                        <a href="#nosotros">Nosotros <div className="linea"></div> </a>
                        <a href="#ilustraciones">Ilustraciones <div className="linea"></div> </a>
                        <a href="#empresa">Empresa <div className="linea"></div> </a>
                        <a href="#precios">Precios <div className="linea"></div> </a>

                    </div>
                </nav>

                <div className="degradado">
                    <div className="contenido-header">
                        <h1 className="_1" ref={this.n1} > bienvenido a Redondo </h1>
                        <p className="_2"  > Una nueva forma de hacer software </p>
                        <div className="containerButtons _3"  >
                            <a href="#nosotros" className="btn btnInicio btnAnimado">Aprende Mas</a>
                        </div>
                    </div>
                </div>


            </header>
        )
    }

}
export default sHeader;